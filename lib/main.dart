import 'package:ejemplo_uno/app/ui/routes/app_routes.dart';
import 'package:ejemplo_uno/app/ui/routes/routes.dart';
import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      initialRoute: Routes.splash,
      routes: routes,
      theme: ThemeData.light(),
    );
  }
}

import 'package:ejemplo_uno/app/ui/routes/routes.dart';

class MenuItem {
  MenuItem({
    required this.title,
    required this.description,
    required this.id,
    required this.routeName,
  });
  final String title;
  final String description;
  final int id;
  final String routeName;
}

List<MenuItem> items = [
  MenuItem(
    title: "Buttons",
    description: "Ejemplos del uso de botones",
    id: 1,
    routeName: Routes.buttons,
  ),
  MenuItem(
    title: "TextField",
    description: "Ejemplos del uso de TextInput",
    id: 2,
    routeName: Routes.texfield,
  ),
  MenuItem(
    title: "Complementos",
    description: "Ejemplos del uso de widgets",
    id: 3,
    routeName: Routes.complementos,
  ),
  MenuItem(
    title: "ListView",
    description: "Ejemplos del uso de ListView",
    id: 4,
    routeName: Routes.listview,
  ),
  MenuItem(
    title: "Stack",
    description: "Ejemplos del uso de Stack",
    id: 5,
    routeName: Routes.stack,
  ),
  MenuItem(
    title: "Imagenes",
    description: "Ejemplos del uso de imagenes",
    id: 6,
    routeName: Routes.imagenes,
  ),
];

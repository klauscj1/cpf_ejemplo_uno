class ArtistaModel {
  String? nombre;
  int? edad;
  String? cancionPrincipal;
  int? cantidadDiscos;
  String? pais;
  int id;

  ArtistaModel({
    required this.id,
    this.nombre,
    this.edad,
    this.cancionPrincipal,
    this.cantidadDiscos,
    this.pais,
  });
}

List<ArtistaModel> artistas = [
  ArtistaModel(
    id: 1,
    nombre: 'Mana',
    cancionPrincipal: 'En el muelle de san blas',
    cantidadDiscos: 12,
    edad: 30,
    pais: 'Mexico',
  ),
  ArtistaModel(
    id: 2,
    nombre: 'Rata Blanca',
    cancionPrincipal: 'La leyende del mago y del hada',
    cantidadDiscos: 11,
    edad: 30,
    pais: 'España',
  ),
  ArtistaModel(
    id: 3,
    nombre: 'Bunbury',
    cancionPrincipal: 'Aunque no sea conmigo',
    cantidadDiscos: 16,
    edad: 20,
    pais: 'España',
  ),
  ArtistaModel(
    id: 4,
    nombre: 'Tercer mundo',
    cancionPrincipal: 'Siempre te ame',
    cantidadDiscos: 10,
    edad: 15,
    pais: 'Ecuador',
  ),
  ArtistaModel(
    id: 5,
    nombre: 'Banda 1',
    cancionPrincipal: 'Banda 1',
    cantidadDiscos: 1,
    edad: 3,
    pais: 'Mexico',
  ),
  ArtistaModel(
    id: 6,
    nombre: 'Mana',
    cancionPrincipal: 'En el muelle de san blas',
    cantidadDiscos: 12,
    edad: 30,
    pais: 'Mexico',
  ),
  ArtistaModel(
    id: 7,
    nombre: 'Rata Blanca',
    cancionPrincipal: 'La leyende del mago y del hada',
    cantidadDiscos: 11,
    edad: 30,
    pais: 'España',
  ),
  ArtistaModel(
    id: 8,
    nombre: 'Bunbury',
    cancionPrincipal: 'Aunque no sea conmigo',
    cantidadDiscos: 16,
    edad: 20,
    pais: 'España',
  ),
  ArtistaModel(
    id: 9,
    nombre: 'Tercer mundo',
    cancionPrincipal: 'Siempre te ame',
    cantidadDiscos: 10,
    edad: 15,
    pais: 'Ecuador',
  ),
  ArtistaModel(
    id: 10,
    nombre: 'Banda 1',
    cancionPrincipal: 'Banda 1',
    cantidadDiscos: 1,
    edad: 3,
    pais: 'Mexico',
  ),
  ArtistaModel(
    id: 11,
    nombre: 'Mana',
    cancionPrincipal: 'En el muelle de san blas',
    cantidadDiscos: 12,
    edad: 30,
    pais: 'Mexico',
  ),
  ArtistaModel(
    id: 12,
    nombre: 'Rata Blanca',
    cancionPrincipal: 'La leyende del mago y del hada',
    cantidadDiscos: 11,
    edad: 30,
    pais: 'España',
  ),
  ArtistaModel(
    id: 13,
    nombre: 'Bunbury',
    cancionPrincipal: 'Aunque no sea conmigo',
    cantidadDiscos: 16,
    edad: 20,
    pais: 'España',
  ),
  ArtistaModel(
    id: 14,
    nombre: 'Tercer mundo',
    cancionPrincipal: 'Siempre te ame',
    cantidadDiscos: 10,
    edad: 15,
    pais: 'Ecuador',
  ),
  ArtistaModel(
    id: 15,
    nombre: 'Banda 1',
    cancionPrincipal: 'Banda 1',
    cantidadDiscos: 1,
    edad: 3,
    pais: 'Mexico',
  ),
];

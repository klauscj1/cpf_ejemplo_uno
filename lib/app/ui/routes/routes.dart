abstract class Routes {
  static const splash = 'splash';
  static const home = 'home';
  static const buttons = 'buttons';
  static const texfield = 'texfield';
  static const complementos = 'complementos';
  static const listview = 'listview';
  static const stack = 'stack';
  static const imagenes = 'imagenes';
}

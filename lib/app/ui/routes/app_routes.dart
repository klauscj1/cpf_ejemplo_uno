import 'package:ejemplo_uno/app/ui/pages/button/button_page.dart';
import 'package:ejemplo_uno/app/ui/pages/complementos/complementos_page.dart';
import 'package:ejemplo_uno/app/ui/pages/home/home_page.dart';
import 'package:ejemplo_uno/app/ui/pages/imagenes/imagenes_page.dart';
import 'package:ejemplo_uno/app/ui/pages/listview/listview_page.dart';
import 'package:ejemplo_uno/app/ui/pages/splash/splash_page.dart';
import 'package:ejemplo_uno/app/ui/pages/stack/stack_page.dart';
import 'package:ejemplo_uno/app/ui/pages/textfield/textfield_page.dart';
import 'package:ejemplo_uno/app/ui/routes/routes.dart';
import 'package:flutter/cupertino.dart';

Map<String, Widget Function(BuildContext)> routes = {
  Routes.splash: (_) => const SplashPage(),
  Routes.home: (_) => const HomePage(),
  Routes.buttons: (_) => const ButtonPage(),
  Routes.texfield: (_) => const TextFieldPage(),
  Routes.complementos: (_) => const ComplementosPage(),
  Routes.listview: (_) => const ListViewPage(),
  Routes.stack: (_) => const StackPage(),
  Routes.imagenes: (_) => const ImaganesPage(),
};

import 'package:flutter/material.dart';

class ButtonPage extends StatelessWidget {
  const ButtonPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Button page"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(),
          ElevatedButton(
            onPressed: () {},
            child: const Text("ElevatedButton"),
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: Colors.orange,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25),
              ),
              elevation: 5,
            ),
            onPressed: () {},
            child: const Text("ElevatedButton custom"),
          ),
          ElevatedButton.icon(
            onPressed: () {},
            icon: const Icon(Icons.camera_enhance),
            label: const Text("ElevatedButton icon"),
          ),
          const Divider(),
          TextButton(
            onPressed: () {},
            child: const Text("TextButton"),
          ),
          TextButton(
            style: TextButton.styleFrom(
                padding: const EdgeInsets.all(15), primary: Colors.orange),
            onPressed: () {},
            child: const Text("TextButton custom"),
          ),
          TextButton.icon(
            onPressed: () {},
            icon: const Icon(Icons.camera),
            label: const Text("TextButton icon"),
          ),
          const Divider(),
          IconButton(
            onPressed: () {},
            icon: const Icon(
              Icons.gps_fixed,
              size: 40,
            ),
            color: Colors.blue,
            splashColor: Colors.orange,
          ),
        ],
      ),
    );
  }
}

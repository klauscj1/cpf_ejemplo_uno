import 'package:flutter/material.dart';

class TextFieldPage extends StatefulWidget {
  const TextFieldPage({Key? key}) : super(key: key);

  @override
  State<TextFieldPage> createState() => _TextFieldPageState();
}

class _TextFieldPageState extends State<TextFieldPage> {
  bool ocultarPassword = true;
  final TextEditingController _controllerText = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("TextField page"),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: ListView(
          children: [
            const Text("TextField Default"),
            const TextField(),
            const SizedBox(
              height: 20,
            ),
            const Text("TextField types"),
            const TextField(
              keyboardType: TextInputType.number,
            ),
            const SizedBox(
              height: 20,
            ),
            const Text("TextField readOnly"),
            const TextField(
              readOnly: true,
            ),
            const SizedBox(
              height: 20,
            ),
            const Text("Obscure text"),
            const TextField(
              obscureText: true,
            ),
            const SizedBox(
              height: 20,
            ),
            TextField(
              controller: _controllerText,
              obscureText: ocultarPassword,
              decoration: InputDecoration(
                hintText: 'Custom style',
                label: const Text("Custom Style"),
                border: const OutlineInputBorder(),
                suffixIcon: IconButton(
                  onPressed: () {
                    ocultarPassword = !ocultarPassword;
                    setState(() {});
                  },
                  icon: Icon(ocultarPassword ? Icons.play_arrow : Icons.stop),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: () {
                if (_controllerText.text == "1234") {
                  //print("Se permite el login");
                  _controllerText.clear();
                } else {
                  // print("Se denega el login");
                }
              },
              child: const Text("Guardar"),
            )
          ],
        ),
      ),
    );
  }
}

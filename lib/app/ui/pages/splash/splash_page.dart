import 'package:ejemplo_uno/app/ui/routes/routes.dart';
import 'package:flutter/material.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    //print("init state");
    _init();
  }

  @override
  Widget build(BuildContext context) {
    //print("build c1");
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: const Center(
        child: Text(
          'Codeteca',
          style: TextStyle(
            fontSize: 35,
            color: Colors.white,
            letterSpacing: 1.5,
          ),
        ),
      ),
    );
  }

  void _init() async {
    await Future.delayed(const Duration(milliseconds: 1800));
    //print("deberia navegar");

    Navigator.pushReplacementNamed(context, Routes.home);

    // Navigator.pushReplacement(
    //   context,
    //   MaterialPageRoute(
    //     builder: (_) => const HomePage(),
    //   ),
    // );
  }
}

import 'package:ejemplo_uno/app/domain/models/artista_model.dart';
import 'package:flutter/material.dart';

class ListViewPage extends StatelessWidget {
  const ListViewPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("ListView"),
      ),
      body: ListView.builder(
        itemCount: artistas.length,
        itemBuilder: (BuildContext context, int index) {
          final artista = artistas[index];
          return ListTile(
            title: Text("${artista.nombre}"),
            subtitle: Text("${artista.cancionPrincipal}"),
            leading: CircleAvatar(
              child: Text(
                "${artista.id}",
              ),
            ),
          );
        },
      ),
    );
  }
}

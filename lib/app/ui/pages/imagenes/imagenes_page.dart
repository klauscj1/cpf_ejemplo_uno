import 'package:flutter/material.dart';

class ImaganesPage extends StatelessWidget {
  const ImaganesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Imagenes"),
      ),
      body: Column(
        children: [
          Container(
            color: Colors.blue,
            width: 300,
            height: 200,
            child: Image.network(
                "https://es.web.img3.acsta.net/newsv7/21/07/06/11/37/3498224.jpg"),
          ),
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Container(
              decoration: BoxDecoration(boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(.5),
                  offset: const Offset(4, 3),
                  blurRadius: 20,
                  spreadRadius: 1,
                )
              ]),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: SizedBox(
                  width: double.infinity,
                  height: 200,
                  child: Image.asset(
                    "assets/images/ejemplo.jpg",
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

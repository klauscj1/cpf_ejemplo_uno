import 'package:ejemplo_uno/app/domain/models/menu_item_model.dart';
import 'package:flutter/material.dart';

import 'home_item_widget.dart';

class MenuItemList extends StatelessWidget {
  const MenuItemList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: items.length,
      itemBuilder: (BuildContext context, int index) {
        return HomeItem(
          title: items[index].title,
          description: items[index].description,
          routeName: items[index].routeName,
          id: items[index].id,
        );
      },
    );
  }
}

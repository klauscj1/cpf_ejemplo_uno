import 'package:flutter/material.dart';

class HomeItem extends StatelessWidget {
  const HomeItem({
    Key? key,
    required this.title,
    required this.description,
    required this.routeName,
    required this.id,
  }) : super(key: key);

  final String title;
  final String description;
  final String routeName;
  final int id;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        title,
        style: const TextStyle(fontWeight: FontWeight.bold),
      ),
      subtitle: Text(description),
      onTap: () {
        Navigator.pushNamed(context, routeName);
      },
      trailing: const Icon(Icons.chevron_right),
      leading: CircleAvatar(
        backgroundColor: Colors.brown,
        child: Text("$id"),
      ),
    );
  }
}

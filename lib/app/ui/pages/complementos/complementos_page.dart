import 'package:flutter/material.dart';

class ComplementosPage extends StatelessWidget {
  const ComplementosPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Complementos page"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 20,
                height: 20,
                color: Colors.yellow,
              ),
              Container(
                width: 20,
                height: 20,
                color: Colors.green,
              ),
              Container(
                width: 20,
                height: 20,
                color: Colors.red,
              ),
            ],
          ),
          const SizedBox(
            height: 30,
          ),
          InkWell(
            onTap: () {
              // print("boton custom");
            },
            child: Container(
              width: 200,
              height: 50,
              decoration: BoxDecoration(
                  color: Colors.amber.shade300,
                  borderRadius: BorderRadius.circular(5),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(.6),
                      offset: const Offset(3, 5),
                      blurRadius: 8,
                    ),
                  ]),
              child: const Center(child: Text("Guardar")),
            ),
          ),
          const SizedBox(
            height: 40,
            child: Center(child: Text("SizedBox")),
          ),
        ],
      ),
    );
  }
}
